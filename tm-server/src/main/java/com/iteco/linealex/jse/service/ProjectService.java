package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.repository.IProjectRepository;
import com.iteco.linealex.jse.api.service.IProjectService;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.Project;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class ProjectService extends AbstractTMService<Project> implements IProjectService {

    public ProjectService(
            @NotNull final IPropertyService propertyService,
            @NotNull final Bootstrap bootstrap
    ) {
        super(propertyService, bootstrap);
    }

    @Nullable
    @Override
    public Project getEntityById(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null || entityId.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findOneByProjectId(entityId);
        }
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAll();
        }
    }

    @Nullable
    @Override
    public Project persist(
            @Nullable final Project project
    ) throws Exception {
        if (project == null) return null;
        if (project.getName() == null || project.getName().isEmpty()) return null;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return null;
        if (project.getUserId() == null || project.getUserId().isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.persist(project);
            sqlSession.commit();
            return project;
        }
    }

    @Override
    public void persist(
            @NotNull final Collection<Project> collection
    ) throws Exception {
        if (collection.isEmpty()) return;
        for (@NotNull final Project project : collection) {
            if (project.getId().isEmpty()) continue;
            if (project.getUserId() == null || project.getUserId().isEmpty()) continue;
            try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                projectRepository.persist(project);
                sqlSession.commit();
            }
        }
    }

    @Override
    public void removeEntity(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null || entityId.isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.remove(entityId);
            sqlSession.commit();
        }
    }

    @Override
    public void removeAllEntities(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeAllByUserId(userId);
            sqlSession.commit();
        }
    }

    @Override
    public void removeAllEntities() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeAll();
            sqlSession.commit();
        }
    }

    @Override
    public void merge(
            @Nullable final Project entity
    ) throws Exception {
        if (entity == null) return;
        if (entity.getName() == null || entity.getName().isEmpty()) return;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.merge(entity);
            sqlSession.commit();
        }
    }

    @Nullable
    @Override
    public Project getEntityByName(
            @Nullable final String entityName
    ) throws Exception {
        if (entityName == null || entityName.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findOneByName(entityName);
        }
    }

    @Nullable
    @Override
    public Project getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (entityName == null || entityName.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findOneByNameAndUserId(userId, entityName);
        }
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllByNameAndUserId(userId, pattern);
        }
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String pattern
    ) throws Exception {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllByName(pattern);
        }
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStartDate() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllSortedByStartDate();
        }
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStartDate(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllSortedByStartDateAndUserId(userId);
        }
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByFinishDate() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllSortedByFinishDate();
        }
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllSortedByFinishDateAndUserId(userId);
        }
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStatus() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllSortedByStatus();
        }
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStatus(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllSortedByStatusAndUserId(userId);
        }
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllByUserId(userId);
        }
    }

}