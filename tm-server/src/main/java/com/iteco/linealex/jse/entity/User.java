package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public final class User extends AbstractEntity {

    @NotNull
    private String login = "unnamed";

    @Nullable
    private String hashPassword;

    @NotNull
    private Role role = Role.ORDINARY_USER;

    public User(@NotNull final String login,
                @NotNull final String hashPassword) {
        this.login = login;
        this.hashPassword = hashPassword;
    }

    @NotNull
    @Override
    public String toString() {
        return "User " + login +
                ",\n     role = " + role +
                ",\n     id = " + super.getId() +
                ",\n     hashPassword = " + hashPassword;
    }

}