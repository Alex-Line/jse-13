package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractEntity {

    @NotNull String userId;

    @NotNull Role role;

    @NotNull Date creationDate = new Date(System.currentTimeMillis());

    @Nullable String signature = null;

    @Override
    public String toString() {
        return "Session{" +
                "\n     userId='" + userId + '\'' +
                ",\n      role=" + role +
                ",\n      creationDate=" + creationDate +
                ",\n      signature='" + signature + '\'';
    }
}