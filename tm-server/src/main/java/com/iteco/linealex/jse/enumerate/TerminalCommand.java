package com.iteco.linealex.jse.enumerate;

@Deprecated
public enum TerminalCommand {
    PROJECT_CREATE("project-create"),
    PROJECT_LIST("project-list"),
    PROJECT_REMOVE("project-remove"),
    PROJECT_CLEAR("project-clear"),
    PROJECT_SELECT("project-select"),
    TASK_SELECT("task-select"),
    TASK_ATTACH("task-attach"),
    TASK_CREATE("task-create"),
    TASK_LIST("task-list"),
    TASK_REMOVE("task-remove"),
    TASK_CLEAR("task-clear"),
    TASK_CLEAR_ALL("task-clear-all"),
    CLEAR_SELECTION("clear-selection"),
    EXIT("exit"),
    HELP("help"),
    DEFAULT("//unknown");

    private String command;

    TerminalCommand(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
}
