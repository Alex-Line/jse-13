package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.repository.IRepository;
import com.iteco.linealex.jse.entity.AbstractEntity;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

@Getter
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    final Connection connection;

    protected AbstractRepository(
            @NotNull final Connection connection) {
        this.connection = connection;
    }

    @Nullable
    abstract protected T operate(
            @Nullable final ResultSet row
    ) throws SQLException;

    public abstract boolean contains(
            @NotNull final String entityId
    ) throws SQLException;

    @NotNull
    @Override
    public abstract Collection<T> findAll() throws SQLException;

    @NotNull
    public abstract Collection<T> findAll(
            @NotNull final String userId
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAll(
            @NotNull final String userId, @NotNull final String projectId
    ) throws SQLException;

    @Nullable
    public abstract T findOne(
            @NotNull final String entityId
    ) throws SQLException;

    @Nullable
    public abstract T findOneByName(
            @NotNull final String login,
            @NotNull final String hashPassword
    ) throws SQLException;

    @Nullable
    @Override
    public abstract T persist(
            @NotNull final T example
    ) throws SQLException;

    @NotNull
    @Override
    public abstract Collection<T> persist(
            @NotNull final Collection<T> collection
    ) throws SQLException;

    @Nullable
    @Override
    public abstract T merge(
            @NotNull final T example
    ) throws SQLException;

    @Nullable
    @Override
    public abstract T remove(
            @NotNull final String entityId
    ) throws SQLException;

    @NotNull
    @Override
    public abstract Collection<T> removeAll() throws SQLException;

    @NotNull
    public abstract Collection<T> removeAll(
            @NotNull final String userId
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> removeAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException;

}