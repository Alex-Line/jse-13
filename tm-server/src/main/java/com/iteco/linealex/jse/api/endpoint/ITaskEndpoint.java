package com.iteco.linealex.jse.api.endpoint;

import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    Task getTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void persistTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "tasks", partName = "tasks") @NotNull final Collection<Task> collection
    ) throws Exception;

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws Exception;

    @WebMethod
    void removeAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @WebMethod
    void removeAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void persistTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @Nullable final Task entity
    ) throws Exception;

    @WebMethod
    void mergeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @Nullable final Task entity
    ) throws Exception;

    @Nullable
    @WebMethod
    Task getTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws Exception;

    @Nullable
    @WebMethod
    Task getTaskByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception;

    @WebMethod
    void removeAllTasksFromProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception;

    @WebMethod
    void attachTaskToProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "task", partName = "task") @Nullable final Task selectedEntity
    ) throws Exception;

    @Nullable
    @WebMethod
    Task getTaskByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStartDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByFinishDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> getAllTasksSortedByStatusWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception;

}