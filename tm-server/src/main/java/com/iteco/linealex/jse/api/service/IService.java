package com.iteco.linealex.jse.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IService<T> {

    T persist(
            @Nullable final T entity
    ) throws Exception;

    void persist(
            @NotNull final Collection<T> collection
    ) throws Exception;

    void merge(
            @Nullable final T entity
    ) throws Exception;

    @Nullable
    T getEntityById(
            @Nullable final String entityId
    ) throws Exception;

    @NotNull
    Collection<T> getAllEntities() throws Exception;

    void removeEntity(
            @Nullable final String entityId
    ) throws Exception;

    void removeAllEntities() throws Exception;

}