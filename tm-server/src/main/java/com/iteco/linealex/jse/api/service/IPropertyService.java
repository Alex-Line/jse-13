package com.iteco.linealex.jse.api.service;

import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    @Nullable
    String getProperty(@Nullable final String propertyName);

}