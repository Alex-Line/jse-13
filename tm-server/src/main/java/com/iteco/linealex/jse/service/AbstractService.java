package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.IService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.AbstractEntity;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@Getter
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final Bootstrap bootstrap;

    public AbstractService(
            @NotNull final IPropertyService propertyService,
            @NotNull final Bootstrap bootstrap
    ) {
        this.propertyService = propertyService;
        this.bootstrap = bootstrap;
    }

    @Nullable
    @Override
    public abstract T getEntityById(
            @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @Override
    public abstract Collection<T> getAllEntities() throws Exception;

    @Override
    public abstract T persist(
            @Nullable final T entity
    ) throws Exception;

    @Override
    public abstract void persist(
            @NotNull final Collection<T> collection
    ) throws Exception;

    @Override
    public abstract void removeEntity(
            @Nullable final String entityId
    ) throws Exception;

    public abstract void removeAllEntities(
            @Nullable final String userId
    ) throws Exception;

    public abstract void removeAllEntities() throws Exception;

}