package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.repository.ITaskRepository;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ITaskService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.Task;
import org.apache.ibatis.exceptions.TooManyResultsException;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class TaskService extends AbstractTMService<Task> implements ITaskService {

    public TaskService(
            @NotNull final IPropertyService propertyService,
            @NotNull final Bootstrap bootstrap
    ) {
        super(propertyService, bootstrap);
    }

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllByUserId(userId);
        }
    }

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) return getAllEntities(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
        }
    }

    @Nullable
    @Override
    public Task getEntityById(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findOneByTaskId(entityId);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntities() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAll();
        }
    }

    @Nullable
    @Override
    public Task persist(
            @Nullable final Task entity
    ) throws Exception {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.persist(entity);
            sqlSession.commit();
            return entity;
        }
    }

    @Override
    public void persist(
            @NotNull final Collection<Task> collection
    ) throws Exception {
        if (collection.isEmpty()) return;
        for (@NotNull final Task task : collection) {
            if (task.getName() == null || task.getName().isEmpty()) continue;
            if (task.getUserId() == null || task.getUserId().isEmpty()) continue;
            try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                taskRepository.persist(task);
                sqlSession.commit();
            }
        }
    }

    @Override
    public void merge(
            @Nullable final Task entity
    ) throws Exception {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.merge(entity);
            sqlSession.commit();
        }
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String entityName
    ) throws Exception {
        if (entityName == null || entityName.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findOneByName(entityName);
        } catch (TooManyResultsException e) {
            throw new Exception("THERE ARE SEVERAL TASKS");
        }
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (entityName == null || entityName.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findOneByNameAndUserId(userId, entityName);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllByNameAndUserId(userId, pattern);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String pattern
    ) throws Exception {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllByName(pattern);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllSortedByStartDate();
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllSortedByStartDateAndUserId(userId);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllSortedByFinishDate();
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllSortedByFinishDateAndUserId(userId);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllSortedByStatus();
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllSortedByStatusAndUserId(userId);
        }
    }

    @Override
    public void removeEntity(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null || entityId.isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.remove(entityId);
            sqlSession.commit();
        }
    }

    @Override
    public void removeAllEntities(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeAllByUserId(userId);
            sqlSession.commit();
        }
    }

    @Override
    public void removeAllEntities() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeAll();
            sqlSession.commit();
        }
    }

    public void removeAllTasksFromProject(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) this.removeAllEntities(userId);
        if (userId == null || userId.isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeAllByUserIdAndProjectId(userId, projectId);
            sqlSession.commit();
        }
    }

    public void attachTaskToProject(
            @Nullable final String projectId,
            @Nullable final Task selectedEntity
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) return;
        if (selectedEntity == null) return;
        selectedEntity.setProjectId(projectId);
        merge(selectedEntity);
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findOneByNameAndUserIdAndProjectId(userId, projectId, taskName);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String pattern
    ) throws Exception {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllByNameAndUserIdAndProjectId(userId, projectId, pattern);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllSortedByStartDateAndUserIdAndProjectId(userId, projectId);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllSortedByFinishDateAndUserIdAndProjectId(userId, projectId);
        }
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllSortedByStatusAndUserIdAndProjectId(userId, projectId);
        }
    }

}