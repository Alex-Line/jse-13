package com.iteco.linealex.jse.endpoint;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.exception.session.InvalidSessionException;
import com.iteco.linealex.jse.exception.session.SessionExpiredException;
import com.iteco.linealex.jse.exception.session.ThereIsNotSuchSessionException;
import com.iteco.linealex.jse.util.ApplicationUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;

@Getter
@Setter
@WebService
@NoArgsConstructor
public class AbstractEndpoint {

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IPropertyService propertyService;

    public AbstractEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IPropertyService propertyService
    ) {
        this.sessionService = sessionService;
        this.propertyService = propertyService;
    }

    @WebMethod
    public void validateSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session userSession
    ) throws Exception {
        if (userSession == null) throw new ThereIsNotSuchSessionException();
        @Nullable final Session session = sessionService.getSession(userSession.getUserId(), userSession.getId());
        if (session == null) throw new ThereIsNotSuchSessionException();
        @Nullable final String userSignature = userSession.getSignature();
        userSession.setSignature(null);
        if (!session.getSignature().equals(ApplicationUtils.getSignature(userSession, propertyService)))
            throw new InvalidSessionException();
        userSession.setSignature(userSignature);
        if (new Date().getTime() - session.getCreationDate().getTime() >
                Long.parseLong(propertyService.getProperty("SESSION_LIFE_TIME"))) throw new SessionExpiredException();
    }

}