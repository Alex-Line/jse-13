package com.iteco.linealex.jse.context;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.api.repository.IProjectRepository;
import com.iteco.linealex.jse.api.repository.ISessionRepository;
import com.iteco.linealex.jse.api.repository.ITaskRepository;
import com.iteco.linealex.jse.api.repository.IUserRepository;
import com.iteco.linealex.jse.api.service.*;
import com.iteco.linealex.jse.endpoint.*;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.TaskManagerException;
import com.iteco.linealex.jse.service.*;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.sql.DataSource;
import javax.xml.ws.Endpoint;

@Getter
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectService projectService = new ProjectService(propertyService, this);

    @NotNull
    private final ITaskService taskService = new TaskService(propertyService, this);

    @NotNull
    private final IUserService userService = new UserService(propertyService, this);

    @NotNull
    private final ISessionService sessionService = new SessionService(propertyService, this);

    @NotNull
    private final IProjectEndpoint projectEndpoint
            = new ProjectEndpoint(sessionService, projectService, propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(sessionService, taskService, propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint
            = new UserEndpoint(sessionService, userService, propertyService, projectService, taskService);

    @NotNull
    final IPropertyEndpoint propertyEndpoint = new PropertyEndpoint(sessionService, propertyService);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService, userService, propertyService);

    @NotNull
    private SqlSessionFactory sqlSessionFactory;

    @NotNull
    public SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }

    @NotNull
    private SqlSessionFactory initSQLSessionFactory() throws Exception {
        @Nullable final String user = propertyService.getProperty("DB_USER");
        @Nullable final String url = propertyService.getProperty("DB_URL");
        @Nullable final String password = propertyService.getProperty("DB_PASSWORD");
        @Nullable final String driver = propertyService.getProperty("DB_DRIVER");
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        @NotNull final Environment environment
                = new Environment("development", new JdbcTransactionFactory(), dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        @Nullable final SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(configuration);
        if (factory == null) throw new TaskManagerException();
        return factory;
    }

    public void start() throws Exception {
        sqlSessionFactory = initSQLSessionFactory();
        createInitialUsers();
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionService?wsdl", sessionEndpoint);
        Endpoint.publish("http://localhost:8080/propertyService?wsdl", propertyEndpoint);
    }

    private void createInitialUsers() {
        try {
            @NotNull final User admin = new User();
            admin.setLogin(propertyEndpoint.getProperty("ADMIN"));
            admin.setHashPassword(TransformatorToHashMD5.getHash(
                    propertyEndpoint.getProperty("ADMIN_PASSWORD"),
                    propertyEndpoint.getProperty("PASSWORD_SALT"),
                    Integer.parseInt(propertyEndpoint.getProperty("PASSWORD_TIMES"))));
            admin.setRole(Role.ADMINISTRATOR);
            userService.persist(admin);
            @NotNull final User user = new User();
            user.setLogin(propertyEndpoint.getProperty("USER"));
            user.setHashPassword(TransformatorToHashMD5.getHash(
                    propertyEndpoint.getProperty("USER_PASSWORD"),
                    propertyEndpoint.getProperty("PASSWORD_SALT"),
                    Integer.parseInt(propertyEndpoint.getProperty("PASSWORD_TIMES"))));
            userService.persist(user);
        } catch (TaskManagerException e) {
            System.out.println(e.getMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

}