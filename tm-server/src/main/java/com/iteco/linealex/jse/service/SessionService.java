package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.repository.ISessionRepository;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.Session;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(
            @NotNull final IPropertyService propertyService,
            @NotNull final Bootstrap bootstrap
    ) {
        super(propertyService, bootstrap);
    }

    @Nullable
    @Override
    public Session getEntityById(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null || entityId.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findOneById(entityId);
        }
    }

    @NotNull
    @Override
    public Collection<Session> getAllEntities() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAll();
        }
    }

    @Nullable
    @Override
    public Session persist(
            @Nullable final Session entity
    ) throws Exception {
        if (entity == null) return null;
        if (entity.getSignature() == null || entity.getSignature().isEmpty()) return null;
        if (entity.getUserId().isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.persist(entity);
            sqlSession.commit();
            return entity;
        }
    }

    @Override
    public void persist(
            @NotNull final Collection<Session> collection
    ) throws Exception {
        if (collection.isEmpty()) return;
        for (@NotNull final Session session : collection) {
            if (session.getSignature() == null || session.getSignature().isEmpty()) continue;
            if (session.getUserId().isEmpty()) continue;
            try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
                @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
                sessionRepository.persist(session);
                sqlSession.commit();
            }
        }
    }

    @Override
    public void removeEntity(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null || entityId.isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.remove(entityId);
            sqlSession.commit();
        }
    }

    @Override
    public void removeAllEntities(
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeAllByUserId(userId);
            sqlSession.commit();
        }
    }

    @Override
    public void removeAllEntities() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeAll();
            sqlSession.commit();
        }
    }

    @Override
    public void merge(
            @Nullable final Session entity
    ) throws Exception {
        if (entity == null) return;
        if (entity.getSignature() == null || entity.getSignature().isEmpty()) return;
        if (entity.getUserId().isEmpty()) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.merge(entity);
            sqlSession.commit();
        }
    }

    @Nullable
    @Override
    public Session getSession(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findOneById(id);
        }
    }

}