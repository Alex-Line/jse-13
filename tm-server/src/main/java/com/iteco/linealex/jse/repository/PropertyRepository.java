package com.iteco.linealex.jse.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

@Getter
@NoArgsConstructor
public class PropertyRepository {

    @NotNull
    final Properties properties = new Properties();

    @NotNull
    final File resourceFile = new File("src/main/resources/config.properties");

    @NotNull FileInputStream inputStream;

    public void init() throws IOException {
        inputStream = new FileInputStream(resourceFile);
        if (!resourceFile.exists()) throw new FileNotFoundException();
        properties.load(inputStream);
    }

}