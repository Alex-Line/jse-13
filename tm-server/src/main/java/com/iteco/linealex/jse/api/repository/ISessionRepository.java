package com.iteco.linealex.jse.api.repository;

import com.iteco.linealex.jse.entity.Session;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ISessionRepository {

    @NotNull
    @Results({
            @Result(property = "id", column = "sessionId", id = true),
            @Result(property = "creationDate", column = "sessionCreationDate"),
            @Result(property = "role", column = "sessionRole"),
            @Result(property = "userId", column = "users_userId")
    })
    @Select("SELECT * from TaskManager.sessions;")
    Collection<Session> findAll() throws Exception;

    @Delete("DELETE FROM TaskManager.sessions " +
            "WHERE sessionId = #{sessionId};")
    void remove(@NotNull final String entityId) throws Exception;

    @NotNull
    @Delete("DELETE FROM TaskManager.session;")
    Collection<Session> removeAll() throws Exception;

    @NotNull
    @Delete("DELETE FROM TaskManager.sessions " +
            "WHERE users_userId = #{userId};")
    Collection<Session> removeAllByUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "sessionId", id = true),
            @Result(property = "creationDate", column = "sessionCreationDate"),
            @Result(property = "role", column = "sessionRole"),
            @Result(property = "userId", column = "users_userId")
    })
    @Select("SELECT * FROM TaskManager.sessions " +
            "WHERE users_userId = #{userId};")
    Collection<Session> findAllByUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @Nullable
    @Results({
            @Result(property = "id", column = "sessionId", id = true),
            @Result(property = "creationDate", column = "sessionCreationDate"),
            @Result(property = "role", column = "sessionRole"),
            @Result(property = "signature", column = "sessionSignature"),
            @Result(property = "userId", column = "users_userId")
    })
    @Select("SELECT * FROM TaskManager.sessions " +
            "WHERE sessionId = #{sessionId};")
    Session findOneById(
            @NotNull @Param("sessionId") final String entityId
    ) throws Exception;

    @Insert("INSERT INTO TaskManager.sessions " +
            "VALUES (#{id}, #{creationDate}, #{role}, #{signature}, #{userId})")
    void persist(
            @NotNull final Session example
    ) throws Exception;

    @Update("UPDATE TaskManager.sessions " +
            "SET sessionsCreationDate = #{creationDate}, sessionRole = #{role}, " +
            "sessionSignature = #{signature}, users_userId = #{userId} " +
            "WHERE sessionId = #{id}")
    void merge(
            @NotNull final Session example
    ) throws Exception;

}