package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.repository.IUserRepository;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.IUserService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.user.*;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final Bootstrap bootstrap
    ) {
        super(propertyService, bootstrap);
    }

    @Nullable
    @Override
    public User getEntityById(@Nullable final String entityId) throws Exception {
        if (entityId == null) return null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findOneByUserId(entityId);
        }
    }

    @NotNull
    @Override
    public Collection<User> getAllEntities() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            Collection<User> collection = userRepository.findAll();
            sqlSession.commit();
            return collection;
        }
    }

    @Nullable
    @Override
    public User logInUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        if (password == null || password.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password,
                getPropertyService().getProperty("PASSWORD_SALT"),
                Integer.parseInt(getPropertyService().getProperty("PASSWORD_TIMES")));
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findOneByLoginAndPassword(login, hashPassword);
        }
    }

    @Override
    public void createUser(
            @Nullable final User user,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) return;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (user == null) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.persist(user);
            sqlSession.commit();
        }
    }

    @Nullable
    @Override
    public User persist(
            @Nullable final User entity
    ) throws Exception {
        if (entity == null) return null;
        if (entity.getLogin().isEmpty()) return null;
        if (entity.getHashPassword() == null || entity.getHashPassword().isEmpty()) return null;
        @Nullable User existingUser = null;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            existingUser = userRepository.findOneByLogin(entity.getLogin());
            sqlSession.commit();
        }
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (existingUser == null) userRepository.persist(entity);
            sqlSession.commit();
            return entity;
        }
    }

    @Override
    public void persist(
            @NotNull final Collection<User> collection
    ) throws Exception {
        removeAllEntities();
        for (@NotNull final User user : collection) {
            if (user.getLogin().isEmpty()) continue;
            if (user.getHashPassword() == null || user.getHashPassword().isEmpty()) continue;
            try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
                @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
                userRepository.persist(user);
                sqlSession.commit();
            }
        }
    }

    @Override
    public void removeEntity(
            @Nullable final String entityId
    ) throws Exception {
        if (entityId == null) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.remove(entityId);
            sqlSession.commit();
        }
    }

    @Override
    public void removeAllEntities(
            @Nullable final String userId
    ) throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.removeAllByUserId(userId);
            sqlSession.commit();
        }
    }

    @Override
    public void removeAllEntities() throws Exception {
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.removeAll();
            sqlSession.commit();
        }
    }

    @Override
    public void merge(
            @Nullable final User entity
    ) throws Exception {
        if (entity == null) return;
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.merge(entity);
            sqlSession.commit();
        }
    }

    public void updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashOldPassword = TransformatorToHashMD5.getHash(oldPassword,
                getPropertyService().getProperty("PASSWORD_SALT"),
                Integer.parseInt(getPropertyService().getProperty("PASSWORD_TIMES")));
        if (selectedUser.getHashPassword() == null
                && !selectedUser.getHashPassword().equals(hashOldPassword)) throw new WrongPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new WrongPasswordException();
        if (newPassword.length() < 8) throw new ShortPasswordException();
        @NotNull final String hashNewPassword = TransformatorToHashMD5.getHash(newPassword,
                getPropertyService().getProperty("PASSWORD_SALT"),
                Integer.parseInt(getPropertyService().getProperty("PASSWORD_TIMES")));
        selectedUser.setHashPassword(hashNewPassword);
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.merge(selectedUser);
            sqlSession.commit();
        }
    }

    @Nullable
    public User getUser(
            @Nullable final String login,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findOneByLogin(login);
        }
    }

    @NotNull
    public Collection<User> getAllUsers(
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) return Collections.EMPTY_LIST;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        try (@NotNull final SqlSession sqlSession = getBootstrap().getSqlSessionFactory().openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findAll();
        }
    }

}