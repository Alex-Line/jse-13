package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.enumerate.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    protected Session operate(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString("sessionId"));
        session.setCreationDate(row.getDate("sessionCreationDate"));
        session.setRole(Role.valueOf(row.getString("sessionRole")));
        session.setSignature(row.getString("sessionSignature"));
        session.setUserId(row.getString("users_userId"));
        return session;
    }

    @Override
    public boolean contains(@NotNull final String entityId) throws SQLException {
        @NotNull final Session session = findOne(entityId);
        if (session != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<Session> findAll() throws SQLException {
        @NotNull final String query = "SELECT * from TaskManager.sessions;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Session> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @Nullable
    @Override
    public Session remove(@NotNull final String entityId) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.sessions WHERE sessionId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, entityId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Session session = null;
        if (resultSet.next()) session = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        if (session == null) return null;
        @NotNull final String removeQuery = "DELETE FROM TaskManager.sessions WHERE sessionId = ?;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.setString(1, entityId);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return session;
    }

    @NotNull
    @Override
    public Collection<Session> removeAll() throws SQLException {
        Collection<Session> collection = findAll();
        @NotNull final String removeQuery = "DELETE FROM TaskManager.session;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Session> removeAll(
            @NotNull final String userId
    ) throws SQLException {
        Collection<Session> collection = findAll(userId);
        @NotNull final String removeQuery = "DELETE FROM TaskManager.sessions WHERE users_userId = ?;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.setString(1, userId);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Session> removeAll(@NotNull String userId, @NotNull String projectId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Session> findAll(
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "select * from TaskManager.sessions where users_userId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Session> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Session> findAll(
            @NotNull final String userId, @NotNull final String projectId
    ) throws SQLException {
        return findAll(userId);
    }

    @Nullable
    @Override
    public Session findOne(
            @NotNull final String entityId
    ) throws SQLException {
        @NotNull final String query = "select * from TaskManager.sessions where sessionId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, entityId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Session session = null;
        if (resultSet.next()) session = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        return session;
    }

    @Nullable
    @Override
    public Session findOneByName(
            @NotNull String login,
            @NotNull String hashPassword
    ) throws SQLException {
        return null;
    }

    @Nullable
    @Override
    public Session persist(
            @NotNull final Session example
    ) throws SQLException {
        @NotNull final String query = "INSERT INTO TaskManager.sessions " +
                "VALUES (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, example.getId());
        preparedStatement.setDate(2, new java.sql.Date(example.getCreationDate().getTime()));
        preparedStatement.setString(3, example.getRole().getDisplayName().toUpperCase());
        preparedStatement.setString(4, example.getSignature());
        preparedStatement.setString(5, example.getUserId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return example;
    }

    @NotNull
    @Override
    public Collection<Session> persist(
            @NotNull final Collection<Session> collection
    ) throws SQLException {
        for (Session example : collection) {
            @NotNull final String query = "INSERT INTO TaskManager.sessions " +
                    "VALUES (?, ?, ?, ?, ?)";
            @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, example.getId());
            preparedStatement.setDate(2, new java.sql.Date(example.getCreationDate().getTime()));
            preparedStatement.setString(3, example.getRole().getDisplayName().toUpperCase());
            preparedStatement.setString(4, example.getSignature());
            preparedStatement.setString(5, example.getUserId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        return collection;
    }

    @Nullable
    @Override
    public Session merge(
            @NotNull final Session example
    ) throws SQLException {
        @NotNull final String query = "UPDATE TaskManager.sessions SET " +
                "sessionsCreationDate=?, sessionRole=?," +
                " sessionSignature=?, users_userId=? where sessionId=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setDate(1, new Date(example.getCreationDate().getTime()));
        preparedStatement.setString(2, example.getRole().getDisplayName().toUpperCase());
        preparedStatement.setString(3, example.getSignature());
        preparedStatement.setString(4, example.getUserId());
        preparedStatement.setString(5, example.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return example;
    }

}