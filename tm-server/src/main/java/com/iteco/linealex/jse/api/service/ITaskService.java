package com.iteco.linealex.jse.api.service;

import com.iteco.linealex.jse.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    void removeAllTasksFromProject(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception;

    public void removeAllEntities(
            @Nullable final String userId
    ) throws Exception;

    public void attachTaskToProject(
            @Nullable final String projectId,
            @Nullable final Task selectedEntity
    ) throws Exception;

    @Nullable
    public Task getEntityByName(
            @NotNull final String taskName
    ) throws Exception;

    @Nullable
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String taskName
    ) throws Exception;

    @Nullable
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskName
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStartDate() throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId,
            @Nullable final String project
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesSortedByFinishDate() throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStatus() throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception;

}