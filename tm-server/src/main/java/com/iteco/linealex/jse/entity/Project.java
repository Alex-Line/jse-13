package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.util.ApplicationUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public final class Project extends AbstractTMEntity implements Serializable {

    @NotNull
    @Override
    public String toString() {
        return "Project: " + name +
                " { ID = " + super.getId() +
                ",\n    description = '" + description + '\'' +
                ",\n    Start Date = " + ApplicationUtils.formatDateToString(dateStart) +
                ",\n    Finish Date = " + ApplicationUtils.formatDateToString(dateFinish) +
                ",\n    Status = " + status.getName() +
                ",\n    User ID = " + userId +
                " }";
    }

}