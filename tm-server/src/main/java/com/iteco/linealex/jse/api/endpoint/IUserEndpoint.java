package com.iteco.linealex.jse.api.endpoint;

import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IUserEndpoint {

    @Nullable
    @WebMethod
    User getUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<User> getAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void persistUser(
            @WebParam(name = "user", partName = "user") @Nullable final User entity
    ) throws Exception;

    @WebMethod
    void persistUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "users", partName = "users") @NotNull final Collection<User> collection
    ) throws Exception;

    @WebMethod
    void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    void removeAllUsersWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @WebMethod
    void removeAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    User logInUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws Exception;

    @WebMethod
    public void logOutUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void createUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "user", partName = "user") @Nullable final User user,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws Exception;

    @WebMethod
    void mergeUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "user", partName = "user") @Nullable final User entity
    ) throws Exception;

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "oldPassword", partName = "oldPassword") @Nullable final String oldPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws Exception;

    @Nullable
    @WebMethod
    User getUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<User> getAllUsersBySelectedUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws Exception;

    @WebMethod
    void loadBinary(
            @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void loadFasterJson(
            @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void loadFasterXml(
            @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void loadJaxbJson(
            @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void loadJaxbXml(
            @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void saveBinary(
            @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void saveFasterJson(
            @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void saveFasterXml(
            @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void saveJaxbJson(
            @Nullable final Session session
    ) throws Exception;

    @WebMethod
    void saveJaxbXml(
            @Nullable final Session session
    ) throws Exception;

}