package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.repository.IRepository;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IRepository<User> {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    protected User operate(
            @Nullable final ResultSet row
    ) throws SQLException {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString("userId"));
        user.setHashPassword(row.getString("userHashPassword"));
        user.setLogin(row.getString("userLogin"));
        user.setRole(Role.valueOf(row.getString("userRole")));
        return user;
    }

    public boolean contains(
            @NotNull final String entityId
    ) throws SQLException {
        @NotNull final User user = findOne(entityId);
        if (user != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<User> findAll() throws SQLException {
        @NotNull final String query = "SELECT * from TaskManager.users;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<User> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @Nullable
    @Override
    public User findOneByName(
            @NotNull final String login,
            @NotNull final String hashPassword
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.users WHERE userLogin =? AND userHashPassword =?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, hashPassword);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable User user = null;
        if (resultSet.next()) user = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        return user;
    }

    @Nullable
    @Override
    public User persist(
            @NotNull final User example
    ) throws SQLException {
        @NotNull final String query = "INSERT INTO TaskManager.users " +
                "VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, example.getId());
        preparedStatement.setString(2, example.getLogin());
        preparedStatement.setString(3, example.getHashPassword());
        preparedStatement.setString(4, example.getRole().getDisplayName().toUpperCase());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return example;
    }

    @NotNull
    @Override
    public Collection<User> persist(
            @NotNull final Collection<User> collection
    ) throws SQLException {
        for (@NotNull final User example : collection) {
            @NotNull final String query = "INSERT INTO TaskManager.users " +
                    "VALUES (?, ?, ?, ?)";
            @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, example.getId());
            preparedStatement.setString(2, example.getHashPassword());
            preparedStatement.setString(3, example.getLogin());
            preparedStatement.setString(4, example.getRole().getDisplayName().toUpperCase());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        return collection;
    }

    @NotNull
    @Override
    public Collection<User> findAll(
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "select * from TaskManager.users where userId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<User> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<User> findAll(
            @NotNull final String userId, @NotNull final String projectId
    ) throws SQLException {
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public User findOne(
            @NotNull final String entityId
    ) throws SQLException {
        @NotNull final String query = "select * from TaskManager.users " +
                "where userId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, entityId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull User user = null;
        if (resultSet.next()) user = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        return user;
    }

    @Nullable
    @Override
    public User merge(
            @NotNull final User example
    ) throws SQLException {
        @NotNull final String query = "UPDATE TaskManager.users SET " +
                "userHashPassword=?, userLogin=?, userRole=?, " +
                "WHERE userId=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, example.getHashPassword());
        preparedStatement.setString(2, example.getLogin());
        preparedStatement.setString(3, example.getRole().getDisplayName().toUpperCase());
        preparedStatement.setString(4, example.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return example;
    }

    @Nullable
    @Override
    public User remove(
            @NotNull final String entityId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.users WHERE userId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, entityId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable User user = null;
        if (resultSet.next()) user = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        if (user == null) return null;
        @NotNull final String removeQuery = "DELETE FROM TaskManager.users WHERE userId = ?;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.setString(1, entityId);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return user;
    }

    @NotNull
    @Override
    public Collection<User> removeAll() throws SQLException {
        Collection<User> collection = findAll();
        @NotNull final String removeQuery = "DELETE FROM TaskManager.users;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<User> removeAll(
            @NotNull final String userId
    ) throws SQLException {
        Collection<User> collection = findAll(userId);
        @NotNull final String removeQuery = "DELETE FROM TaskManager.users WHERE userId = ?;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.setString(1, userId);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<User> removeAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException {
        return Collections.EMPTY_LIST;
    }

}