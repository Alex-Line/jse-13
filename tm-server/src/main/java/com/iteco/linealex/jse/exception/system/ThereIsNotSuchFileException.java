package com.iteco.linealex.jse.exception.system;

import com.iteco.linealex.jse.exception.TaskManagerException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class ThereIsNotSuchFileException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "[THERE IS NOT SUCH FILE]";
    }

}