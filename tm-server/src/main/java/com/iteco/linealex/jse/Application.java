package com.iteco.linealex.jse;

import com.iteco.linealex.jse.context.Bootstrap;
import org.jetbrains.annotations.NotNull;

public final class Application {

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}