package com.iteco.linealex.jse.exception.user;

import com.iteco.linealex.jse.exception.TaskManagerException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class ShortPasswordException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "ENTERED PASSWORD IS TO SHORT. COMMAND WAS INTERRUPTED\n";
    }

}