package com.iteco.linealex.jse.exception.session;

import com.iteco.linealex.jse.exception.TaskManagerException;
import org.jetbrains.annotations.NotNull;

public class ThereIsNotSuchSessionException extends TaskManagerException {

    @Override
    public @NotNull String getMessage() {
        return "THERE IS NOT SUCH SESSION. YOU HAVE TO LOG IN AGAIN";
    }

}