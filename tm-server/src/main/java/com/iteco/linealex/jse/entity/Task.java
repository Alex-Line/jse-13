package com.iteco.linealex.jse.entity;

import com.iteco.linealex.jse.enumerate.Status;
import com.iteco.linealex.jse.util.ApplicationUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractTMEntity implements Serializable {

    @Nullable
    private String projectId = null;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    @Override
    public String toString() {
        return "Task " + name + " {" +
                "ID = " + super.getId() +
                ", \n    description='" + description + '\'' +
                ", \n    Start date = " + ApplicationUtils.formatDateToString(dateStart) +
                ", \n    Finish date = " + ApplicationUtils.formatDateToString(dateFinish) +
                ", \n    Status = " + status.getName() +
                ", \n    PROJECT_ID = " + projectId +
                ", \n    User ID = " + userId +
                '}';
    }

}