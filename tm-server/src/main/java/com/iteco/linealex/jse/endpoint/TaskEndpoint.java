package com.iteco.linealex.jse.endpoint;

import com.iteco.linealex.jse.api.endpoint.ITaskEndpoint;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.api.service.ITaskService;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.user.LowAccessLevelException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.Collections;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final ITaskService taskService,
            @NotNull final IPropertyService propertyService
    ) {
        super(sessionService, propertyService);
        this.taskService = taskService;
    }

    @Nullable
    @Override
    @WebMethod
    public Task getTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        @Nullable final Task task = taskService.getEntityById(entityId);
        if (task == null) return null;
        if (session == null) return null;
        if (task.getUserId() == null) return null;
        if (task.getUserId().equals(session.getUserId()) || session.getRole().equals(Role.ADMINISTRATOR)) {
            return task;
        }
        return null;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        if(session.getRole() != Role.ADMINISTRATOR) return Collections.EMPTY_LIST;
        validateSession(session);
        return taskService.getAllEntities();
    }

    @Override
    @WebMethod
    public void persistTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "tasks", partName = "tasks") @NotNull final Collection<Task> collection
    ) throws Exception {
        validateSession(session);
        taskService.persist(collection);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        taskService.removeEntity(entityId);
    }

    @Override
    @WebMethod
    public void removeAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        taskService.removeAllEntities(userId);
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        if(session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        taskService.removeAllEntities();
    }

    @Override
    @WebMethod
    public void persistTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @Nullable final Task entity
    ) throws Exception {
        validateSession(session);
        if(entity == null) throw new Exception("Null task");
        if(session == null) throw new Exception("Null session");
        if(entity.getUserId() == null) throw new Exception("Null user id");
        if(!entity.getUserId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        taskService.persist(entity);
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @Nullable final Task entity
    ) throws Exception {
        validateSession(session);
        taskService.merge(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public Task getTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws Exception {
        validateSession(session);
        @Nullable final Task task = taskService.getEntityByName(entityName);
        if (task == null) return null;
        if (session == null) return null;
        if (task.getUserId() == null) return null;
        if (!task.getUserId().equals(session.getUserId()) || session.getRole() != Role.ADMINISTRATOR) {
            throw new LowAccessLevelException();
        }
        return task;
    }

    @Nullable
    @Override
    @WebMethod
    public Task getTaskByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws Exception {
        validateSession(session);
        @Nullable final Task task = taskService.getEntityByName(userId, entityName);
        if (task == null) return null;
        if (session == null) return null;
        if (task.getUserId() == null) return null;
        if (task.getUserId().equals(session.getUserId()) || session.getRole().equals(Role.ADMINISTRATOR)) {
            return task;
        }
        return null;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesByName(userId, pattern);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesByName(pattern);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStartDate();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStartDate(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByFinishDate();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByFinishDate(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStatus();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStatus(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntities(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntities(userId, projectId);
    }

    @Override
    @WebMethod
    public void removeAllTasksFromProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        taskService.removeAllTasksFromProject(userId, projectId);
    }

    @Override
    @WebMethod
    public void attachTaskToProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "task", partName = "task") @Nullable final Task selectedEntity
    ) throws Exception {
        validateSession(session);
        taskService.attachTaskToProject(projectId, selectedEntity);
    }

    @Nullable
    @Override
    @WebMethod
    public Task getTaskByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName
    ) throws Exception {
        validateSession(session);
        return taskService.getEntityByName(userId, projectId, taskName);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesByName(userId, projectId, pattern);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStartDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStartDate(userId, projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByFinishDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByFinishDate(userId, projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> getAllTasksSortedByStatusWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return taskService.getAllEntitiesSortedByStatus(userId, projectId);
    }

}