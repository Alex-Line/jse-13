package com.iteco.linealex.jse.exception.user;

import com.iteco.linealex.jse.exception.TaskManagerException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class LoginIncorrectException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "YOU ENTER WRONG OR EMPTY LOGIN. COMMAND WAS INTERRUPTED\n";
    }

}