package com.iteco.linealex.jse.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.iteco.linealex.jse.enumerate.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
public class AbstractTMEntity extends AbstractEntity {

    @Nullable
    String name = "unnamed";

    @Nullable
    String description = "";

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateStart = new Date();

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateFinish = new Date();

    @Nullable
    String userId = null;

    @NotNull
    Status status = Status.PLANNED;

}