package com.iteco.linealex.jse.endpoint;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.iteco.linealex.jse.api.endpoint.IUserEndpoint;
import com.iteco.linealex.jse.api.service.*;
import com.iteco.linealex.jse.dto.Domain;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.system.ThereIsNotSuchFileException;
import com.iteco.linealex.jse.exception.user.LowAccessLevelException;
import com.iteco.linealex.jse.exception.user.UserIsNotExistException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.Collection;
import java.util.Collections;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public UserEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        super(sessionService, propertyService);
        this.userService = userService;
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Nullable
    @Override
    @WebMethod
    public User getUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String entityId
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @Nullable final User user = userService.getEntityById(entityId);
        if (user == null) throw new UserIsNotExistException();
        if (!user.getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return user;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<User> getAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        return userService.getAllEntities();
    }

    @Override
    @WebMethod
    public void persistUser(
            @WebParam(name = "user", partName = "user") @Nullable final User entity
    ) throws Exception {
        if (entity == null) throw new Exception("Null user!");
        userService.persist(entity);
    }

    @Override
    @WebMethod
    public void persistUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "users", partName = "users") @NotNull final Collection<User> collection
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        userService.persist(collection);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        userService.removeEntity(userId);
    }

    @Override
    @WebMethod
    public void removeAllUsersWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        userService.removeEntity(userId);
    }

    @Override
    @WebMethod
    public void removeAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        userService.removeAllEntities();
    }

    @Nullable
    @Override
    @WebMethod
    public User logInUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws Exception {
        return userService.logInUser(login, password);
    }

    @Override
    @WebMethod
    public void logOutUser(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        getSessionService().removeEntity(session.getId());
    }

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "user", partName = "user") @Nullable final User user,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        userService.createUser(user, selectedUser);
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "user", partName = "user") @Nullable final User entity
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (entity == null) throw new UserIsNotExistException();
        if (session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        userService.merge(entity);
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "oldPassword", partName = "oldPassword") @Nullable final String oldPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (selectedUser == null) throw new UserIsNotExistException();
        if (!selectedUser.getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        userService.updateUserPassword(oldPassword, newPassword, selectedUser);
    }

    @Nullable
    @Override
    @WebMethod
    public User getUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @Nullable final User user = userService.getUser(login, selectedUser);
        if (user == null) throw new UserIsNotExistException();
        if (!user.getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return user;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<User> getAllUsersBySelectedUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final User selectedUser
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        return userService.getAllUsers(selectedUser);
    }

    @Override
    @WebMethod
    public void loadBinary(
            @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @NotNull final File savedFile = new File(propertyService.getProperty("SAVE_PATH") + "data.bin");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final FileInputStream inputStream = new FileInputStream(savedFile);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
        if (domain.getSessions() == null) getSessionService().persist(Collections.EMPTY_LIST);
        else getSessionService().persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void loadFasterJson(
            @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @NotNull final File savedFile = new File(propertyService.getProperty("SAVE_PATH") + "data.json");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savedFile, Domain.class);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
    }

    @Override
    @WebMethod
    public void loadFasterXml(
            @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @NotNull final File savedFile = new File(propertyService.getProperty("SAVE_PATH") + "data.xml");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savedFile, Domain.class);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
    }

    @Override
    @WebMethod
    public void loadJaxbJson(
            @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @NotNull final File savedFile = new File(propertyService.getProperty("SAVE_PATH") + "data.json");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        System.out.println(System.getProperty("java.classpath"));
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(savedFile);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
    }

    @Override
    @WebMethod
    public void loadJaxbXml(
            @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @NotNull final File savedFile = new File(propertyService.getProperty("SAVE_PATH") + "data.xml");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(savedFile);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
    }

    @Override
    @WebMethod
    public void saveBinary(
            @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService, taskService, getSessionService());
        @Nullable final String savePath = propertyService.getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.bin");
        @NotNull final FileOutputStream outputStream = new FileOutputStream(saveFile);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.flush();
        objectOutputStream.close();
    }

    @Override
    @WebMethod
    public void saveFasterJson(
            @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService, taskService, getSessionService());
        @Nullable final String savePath = propertyService.getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.json");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
    }

    @Override
    @WebMethod
    public void saveFasterXml(
            @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService, taskService, getSessionService());
        @Nullable final String savePath = propertyService.getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.xml");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
    }

    @Override
    @WebMethod
    public void saveJaxbJson(
            @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService, taskService, getSessionService());
        @Nullable final String savePath = propertyService.getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.json");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
    }

    @Override
    @WebMethod
    public void saveJaxbXml(
            @Nullable Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService, taskService, getSessionService());
        @Nullable final String savePath = propertyService.getProperty("SAVE_PATH");
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.xml");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
    }

}