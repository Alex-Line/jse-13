package com.iteco.linealex.jse.endpoint;

import com.iteco.linealex.jse.api.endpoint.IProjectEndpoint;
import com.iteco.linealex.jse.api.service.IProjectService;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.user.LowAccessLevelException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    public ProjectEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IProjectService projectService,
            @NotNull final IPropertyService propertyService
    ) {
        super(sessionService, propertyService);
        this.projectService = projectService;
    }

    @Nullable
    @Override
    @WebMethod
    public Project getProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String entityId
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @Nullable final Project project = projectService.getEntityById(entityId);
        if (project == null) return null;
        if (project.getUserId() == null) return null;
        if (project.getUserId().equals(session.getUserId()) || session.getRole().equals(Role.ADMINISTRATOR)) {
            return project;
        }
        return null;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        return projectService.getAllEntities();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        return projectService.getAllEntities(userId);
    }

    @Override
    @WebMethod
    public void persistProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projects", partName = "projects") @NotNull final Collection<Project> collection
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        projectService.persist(collection);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String entityId
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        @Nullable final Project project = projectService.getEntityById(entityId);
        if (project == null) throw new Exception("There is not such project!");
        if (project.getUserId() == null) throw new Exception("Null user id!");
        if (!project.getUserId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        projectService.removeEntity(entityId);
    }

    @Override
    @WebMethod
    public void removeAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR && !session.getUserId().equals(userId))
            throw new LowAccessLevelException();
        projectService.removeAllEntities(userId);
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        projectService.removeAllEntities();
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "project", partName = "project") @Nullable final Project entity
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        projectService.persist(entity);
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "project", partName = "project") @Nullable final Project entity
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (entity == null) throw new Exception("Null project!");
        if (!session.getUserId().equals(entity.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        projectService.merge(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public Project getProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String entityName
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (entityName == null) throw new Exception("Null project name!");
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        @Nullable final Project project = projectService.getEntityByName(entityName);
        if (project == null) throw new Exception("There is no such project!");
        return project;
    }

    @Nullable
    @Override
    @WebMethod
    public Project getProjectByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String entityName
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (entityName == null) throw new Exception("Null project name!");
        if (userId == null) throw new Exception(("Null user id"));
        @Nullable final Project project = projectService.getEntityByName(userId, entityName);
        if (project == null) throw new Exception("There is no such project!");
        if (!project.getUserId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return project;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (pattern == null) throw new Exception("Null pattern for search!");
        if (userId == null) throw new Exception(("Null user id"));
        return projectService.getAllEntitiesByName(userId, pattern);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        if (pattern == null) throw new Exception("Null pattern for search!");
        if (session == null) throw new Exception("Null session");
        return projectService.getAllEntitiesByName(pattern);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        return projectService.getAllEntitiesSortedByStartDate();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (userId == null) throw new Exception("Null user id");
        if (!userId.equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return projectService.getAllEntitiesSortedByStartDate(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        return projectService.getAllEntitiesSortedByFinishDate();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (userId == null) throw new Exception("Null user id");
        if (!userId.equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return projectService.getAllEntitiesSortedByFinishDate(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        return projectService.getAllEntitiesSortedByStatus();
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> getAllProjectsSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        if (session == null) throw new Exception("Null session");
        validateSession(session);
        if (userId == null) throw new Exception("Null user id");
        if (!userId.equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return projectService.getAllEntitiesSortedByStatus(userId);
    }

}