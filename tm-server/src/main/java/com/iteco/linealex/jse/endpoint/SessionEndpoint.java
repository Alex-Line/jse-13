package com.iteco.linealex.jse.endpoint;

import com.iteco.linealex.jse.api.endpoint.ISessionEndpoint;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.api.service.IUserService;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.exception.user.UserIsNotExistException;
import com.iteco.linealex.jse.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    final IUserService userService;

    public SessionEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        super(sessionService, propertyService);
        this.userService = userService;
    }

    @Override
    @WebMethod
    public Session createSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception {
        @NotNull final Session session = new Session();
        @Nullable final User user = userService.logInUser(login, password);
        if (user == null) throw new UserIsNotExistException();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setCreationDate(new Date());
        session.setSignature(ApplicationUtils.getSignature(session, getPropertyService()));
        getSessionService().persist(session);
        return session;
    }

    @Override
    @WebMethod
    public void removeSession(
            @WebParam(name = "sessionId", partName = "sessionId") @NotNull final String sessionId
    ) throws Exception {
        getSessionService().removeEntity(sessionId);
    }

    @Nullable
    @Override
    @WebMethod
    public Session findSession(
            @WebParam(name = "sessionId", partName = "sessionId") @Nullable final String sessionId
    ) throws Exception {
        if (sessionId == null) return null;
        return getSessionService().getEntityById(sessionId);
    }

}