package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.repository.IRepository;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.enumerate.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class ProjectRepository extends AbstractTMRepository<Project> implements IRepository<Project> {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    protected Project operate(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString("projectId"));
        project.setDescription(row.getString("projectDescription"));
        project.setName(row.getString("projectName"));
        project.setDateStart(row.getDate("projectStartDate"));
        project.setDateFinish(row.getDate("projectFinishDate"));
        project.setUserId(row.getString("users_userId"));
        project.setStatus(Status.valueOf(row.getString("projectStatus")));
        return project;
    }

    @Override
    public boolean contains(@NotNull final String entityId) throws SQLException {
        @Nullable final Project project = findOne(entityId);
        if (project != null) return true;
        return false;
    }

    @Override
    public boolean contains(
            @NotNull final String name,
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects where users_userId = ? AND projectName = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, name);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final Project project = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        if (project != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<Project> findAll() throws SQLException {
        @NotNull final String query = "SELECT * from TaskManager.projects;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Project> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects where users_userId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Project> findAll(
            @NotNull final String userId, @NotNull final String projectId
    ) throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects where users_userId = ? AND projectId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @Nullable
    @Override
    public Project findOne(@NotNull String entityId) throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects where projectId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, entityId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Project project = null;
        if (resultSet.next()) project = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    ) throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects where users_userId = ? AND " +
                "projectName LIKE '%'?'%' AND projectDescription LIKE '%'?'%';";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, pattern);
        preparedStatement.setString(3, pattern);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Project> findAllByName(@NotNull final String pattern) throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects where " +
                "projectName LIKE '%'?'%' AND projectDescription LIKE '%'?'%';";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, pattern);
        preparedStatement.setString(2, pattern);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Project> findAllByName(
            @NotNull final String pattern,
            @NotNull final String projectId,
            @NotNull final String userId
    ) throws SQLException {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStartDate(
            @NotNull final String projectId,
            @NotNull final String userId
    ) throws SQLException {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStartDate(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects where users_userId = ? " +
                "ORDER BY projectStartDate;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStartDate() throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects " +
                "ORDER BY projectStartDate;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByFinishDate(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByFinishDate(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects where users_userId = ? " +
                "ORDER BY projectFinishDate;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByFinishDate() throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects " +
                "ORDER BY projectFinishDate;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStatus(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStatus(
            @NotNull final String userId
    ) throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects where users_userId = ? " +
                "ORDER BY FIELD(projectStatus, 'PLANNED', 'PROCESSING', 'DONE');";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStatus() throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects " +
                "ORDER BY FIELD(projectStatus, 'PLANNED', 'PROCESSING', 'DONE');";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> list = new ArrayList<>();
        while (resultSet.next()) list.add(operate(resultSet));
        preparedStatement.close();
        resultSet.close();
        return list;
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String entityName) throws SQLException {
        @NotNull final String query = "select * from TaskManager.projects where projectName = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, entityName);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Project project = null;
        if (resultSet.next()) project = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        return project;
    }

    @Nullable
    @Override
    public Project findOneByName(
            @NotNull final String userId,
            @NotNull final String entityName
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.projects " +
                "WHERE users_userId = ? AND projectName = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, entityName);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Project project = null;
        if (resultSet.next()) project = operate(resultSet);
        System.out.println(project);
        preparedStatement.close();
        resultSet.close();
        return project;
    }

    @Nullable
    @Override
    public Project findOneByName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String entityName
    ) {
        return null;
    }

    @NotNull
    @Override
    public Project persist(
            @NotNull final Project example
    ) throws SQLException {
        @NotNull final String query = "INSERT INTO TaskManager.projects " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, example.getId());
        preparedStatement.setString(2, example.getName());
        preparedStatement.setString(3, example.getDescription());
        preparedStatement.setString(4, example.getStatus().getName().toUpperCase());
        preparedStatement.setDate(5, new Date(example.getDateStart().getTime()));
        preparedStatement.setDate(6, new Date(example.getDateFinish().getTime()));
        preparedStatement.setString(7, example.getUserId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return example;
    }

    @NotNull
    @Override
    public Collection<Project> persist(
            @NotNull final Collection<Project> collection
    ) throws SQLException {
        for (Project example : collection) {
            @NotNull final String query = "INSERT INTO TaskManager.projects " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)";
            @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, example.getId());
            preparedStatement.setString(2, example.getDescription());
            preparedStatement.setDate(3, new Date(example.getDateFinish().getTime()));
            preparedStatement.setString(4, example.getName());
            preparedStatement.setDate(5, new Date(example.getDateStart().getTime()));
            preparedStatement.setString(6, example.getStatus().getName().toUpperCase());
            preparedStatement.setString(7, example.getUserId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        return collection;
    }

    @Nullable
    @Override
    public Project merge(@NotNull final Project example) throws SQLException {
        @NotNull final String query = "UPDATE TaskManager.projects SET " +
                "projectDescription=?, projectFinishDate=?, projectName=?," +
                " projectStartDate=?, projectStatus=?, users_userId=? where projectId=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, example.getDescription());
        preparedStatement.setDate(2, new Date(example.getDateFinish().getTime()));
        preparedStatement.setString(3, example.getName());
        preparedStatement.setDate(4, new Date(example.getDateStart().getTime()));
        preparedStatement.setString(5, example.getStatus().getName().toUpperCase());
        preparedStatement.setString(6, example.getUserId());
        preparedStatement.setString(7, example.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return example;
    }

    @Nullable
    @Override
    public Project remove(
            @NotNull final String entityId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM TaskManager.projects WHERE projectId = ?;";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, entityId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Project project = null;
        if (resultSet.next()) project = operate(resultSet);
        preparedStatement.close();
        resultSet.close();
        if (project == null) return null;
        @NotNull final String removeQuery = "DELETE FROM TaskManager.projects WHERE projectId = ?;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.setString(1, entityId);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> removeAll() throws SQLException {
        Collection<Project> collection = findAll();
        @NotNull final String removeQuery = "DELETE FROM TaskManager.projects;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> removeAll(
            @NotNull final String userId
    ) throws SQLException {
        Collection<Project> collection = findAll(userId);
        @NotNull final String removeQuery = "DELETE FROM TaskManager.projects WHERE users_userId = ?;";
        @NotNull final PreparedStatement removePreparedStatement = getConnection().prepareStatement(removeQuery);
        removePreparedStatement.setString(1, userId);
        removePreparedStatement.executeUpdate();
        removePreparedStatement.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> removeAll(@NotNull String userId, @NotNull String projectId) {
        return Collections.EMPTY_LIST;
    }

}