package com.iteco.linealex.jse.api.service;

import com.iteco.linealex.jse.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IProjectService extends IService<Project> {

    @NotNull
    Collection<Project> getAllEntities(
            @Nullable final String userId
    ) throws Exception;

    @Nullable
    Project getEntityByName(
            @Nullable final String entityName
    ) throws Exception;

    @Nullable
    Project getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) throws Exception;

    void removeAllEntities(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    Collection<Project> getAllEntitiesByName(
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    Collection<Project> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    Collection<Project> getAllEntitiesSortedByStartDate() throws Exception;

    @NotNull
    Collection<Project> getAllEntitiesSortedByStartDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    Collection<Project> getAllEntitiesSortedByFinishDate() throws Exception;

    @NotNull
    Collection<Project> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    Collection<Project> getAllEntitiesSortedByStatus() throws Exception;

    @NotNull
    Collection<Project> getAllEntitiesSortedByStatus(
            @Nullable final String userId
    ) throws Exception;

}