package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.api.endpoint.User;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GetAllUsersIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @NotNull
    private Session adminSession;

    @NotNull
    private Session userSession;

    @Test
    void GetAllUsersPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final User newUser = new User();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @NotNull final List<User> collection =
                new ArrayList<>(bootstrap.getUserEndpoint().getAllUsers(adminSession));
        assertEquals(collection.size(), 3);
        Collections.sort(collection, Comparator.comparing(User::getLogin));
        assertEquals(collection.get(0).getId(), admin.getId());
        assertEquals(collection.get(1).getId(), newUser.getId());
        bootstrap.getUserEndpoint().removeUserById(bootstrap.getSession(), newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllUsersNegative1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final User newUser = new User();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getAllUsers(null);
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}