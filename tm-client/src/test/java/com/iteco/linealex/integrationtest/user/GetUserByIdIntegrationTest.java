package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.api.endpoint.User;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GetUserByIdIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private User admin;

    @NotNull
    private Session adminSession;

    @Test
    void getUserByIdPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final User newUser = new User();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @NotNull final User result = bootstrap.getUserEndpoint().getUserById(adminSession, newUser.getId());
        assertEquals(result.getId(), newUser.getId());
        assertEquals(result.getHashPassword(), newUser.getHashPassword());
        assertEquals(result.getLogin(), newUser.getLogin());
        assertEquals(result.getRole(), newUser.getRole());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void getUserByIdNegative1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final User newUser = new User();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserById(null, newUser.getId());
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void getUserByIdNegative2() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final User newUser = new User();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserById(adminSession, UUID.randomUUID().toString());
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}