package com.iteco.linealex.integrationtest.session;

import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.api.endpoint.User;
import com.iteco.linealex.jse.context.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class FindSessionIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @NotNull
    private Session adminSession;

    @NotNull
    private Session userSession;

    @Test
    void createSessionPositive1() throws Exception {
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertNotNull(adminSession);
        @Nullable final Session session = bootstrap.getSessionEndpoint().findSession(adminSession.getId());
        assertNotNull(session);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void createSessionPositive2() throws Exception {
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertNotNull(userSession);
        @Nullable final Session session = bootstrap.getSessionEndpoint().findSession(userSession.getId());
        assertNotNull(session);
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

    @Test
    void createSessionNegative1() throws Exception {
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertNotNull(adminSession);
        @Nullable final Session session = bootstrap.getSessionEndpoint().findSession(null);
        assertNull(session);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void createSessionNegative2() throws Exception {
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertNotNull(userSession);
        @Nullable final Session session = bootstrap.getSessionEndpoint().findSession(UUID.randomUUID().toString());
        assertNull(session);
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

}