package com.iteco.linealex.integrationtest.project;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GetAllProjectsSortedByStartDateIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @NotNull
    private Session adminSession;

    @NotNull
    private Session userSession;

    @Test
    void GetAllProjectsSortedByStartDatePositive1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating new projects
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(111111811111L)));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(9999999999999L)));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final Project projectTest2 = new Project();
        projectTest2.setUserId(admin.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(999999999999L)));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(111167111111L)));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);

        @NotNull final Project projectTest3 = new Project();
        projectTest3.setUserId(admin.getId());
        projectTest3.setId(UUID.randomUUID().toString());
        projectTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(9999999999999L)));
        projectTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(4747387636474L)));
        projectTest3.setName("projectTest3");
        projectTest3.setDescription("projectTest3");
        projectTest3.setStatus(Status.PROCESSING);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest2);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest3);
        @NotNull final List<Project> collection = new ArrayList<>(bootstrap
                .getProjectEndpoint().getAllProjectsSortedByStartDate(adminSession));
        assertFalse(collection.isEmpty());
        assertEquals(3, collection.size());
        assertEquals(collection.get(0).getId(), projectTest.getId());
        assertEquals(collection.get(2).getId(), projectTest3.getId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllProjectsSortedByStartDatePositive2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        /**
         * Creating new projects
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(111111811111L)));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(9999999999999L)));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final Project projectTest2 = new Project();
        projectTest2.setUserId(user.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(999999999999L)));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(111167111111L)));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);

        @NotNull final Project projectTest3 = new Project();
        projectTest3.setUserId(user.getId());
        projectTest3.setId(UUID.randomUUID().toString());
        projectTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(9999999999999L)));
        projectTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(4747387636474L)));
        projectTest3.setName("projectTest3");
        projectTest3.setDescription("projectTest3");
        projectTest3.setStatus(Status.PROCESSING);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest2);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest3);
        @NotNull final List<Project> collection = new ArrayList<>(bootstrap
                .getProjectEndpoint().getAllProjectsSortedByStartDate(userSession));
        assertFalse(collection.isEmpty());
        assertEquals(3, collection.size());
        assertEquals(collection.get(0).getId(), projectTest.getId());
        assertEquals(collection.get(2).getId(), projectTest3.getId());
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint().getAllProjects(userSession);
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllProjectsSortedByStartDateNegative1() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        /**
         * Creating new projects
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(111111811111L)));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(9999999999999L)));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final Project projectTest2 = new Project();
        projectTest2.setUserId(user.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(999999999999L)));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(111167111111L)));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);

        @NotNull final Project projectTest3 = new Project();
        projectTest3.setUserId(user.getId());
        projectTest3.setId(UUID.randomUUID().toString());
        projectTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(9999999999999L)));
        projectTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(4747387636474L)));
        projectTest3.setName("projectTest3");
        projectTest3.setDescription("projectTest3");
        projectTest3.setStatus(Status.PROCESSING);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest2);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest3);
        @NotNull final List<Project> collection = new ArrayList<>(bootstrap
                .getProjectEndpoint().getAllProjectsSortedByStartDate(userSession));
        assertFalse(collection.isEmpty());
        assertEquals(3, collection.size());
        assertNotEquals(collection.get(2).getId(), projectTest.getId());
        assertNotEquals(collection.get(0).getId(), projectTest3.getId());
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint().getAllProjects(userSession);
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllProjectsSortedByStartDateNegative2() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating new projects
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(111111811111L)));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(9999999999999L)));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final Project projectTest2 = new Project();
        projectTest2.setUserId(admin.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(999999999999L)));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(111167111111L)));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);

        @NotNull final Project projectTest3 = new Project();
        projectTest3.setUserId(admin.getId());
        projectTest3.setId(UUID.randomUUID().toString());
        projectTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(9999999999999L)));
        projectTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(4747387636474L)));
        projectTest3.setName("projectTest3");
        projectTest3.setDescription("projectTest3");
        projectTest3.setStatus(Status.PROCESSING);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest2);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest3);
        @NotNull final List<Project> collection = new ArrayList<>(bootstrap
                .getProjectEndpoint().getAllProjectsSortedByStartDate(adminSession));
        assertFalse(collection.isEmpty());
        assertEquals(3, collection.size());
        assertNotEquals(collection.get(1).getId(), projectTest.getId());
        assertNotEquals(collection.get(0).getId(), projectTest3.getId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}