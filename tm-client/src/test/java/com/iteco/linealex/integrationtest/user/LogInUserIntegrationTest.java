package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.api.endpoint.User;
import com.iteco.linealex.jse.context.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LogInUserIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @NotNull
    private Session adminSession;

    @NotNull
    private Session userSession;

    @Test
    void LoginUserPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void LoginUserPositive2() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

    @Test
    void LoginUserNegative1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            adminSession = bootstrap.getSessionEndpoint().createSession("admn",
                    bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    void LoginUserNegative2() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            userSession = bootstrap.getSessionEndpoint().createSession(null,
                    bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        });
        assertNotNull(thrown.getMessage());
    }

}