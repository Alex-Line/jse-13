package com.iteco.linealex.integrationtest.project;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.lang.Exception;
import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PersistProjectIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @NotNull
    private Session adminSession;

    @NotNull
    private Session userSession;

    @Test
    void persisProjectPositive1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating a new project
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest");
        projectTest.setDescription("projectTest");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        @NotNull final Project returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(adminSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeProject(adminSession, returnedProject.getId());
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void persisProjectPositive2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        /**
         * Creating a new project
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("project2");
        projectTest.setDescription("project2");
        projectTest.setStatus(Status.PROCESSING);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        @NotNull final Project returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(userSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void persisProjectPositive3() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        /**
         * Creating a new project
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("project3");
        projectTest.setDescription("project3");
        projectTest.setStatus(Status.PROCESSING);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final Project returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(adminSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void persisProjectNegative1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        /**
         * Creating a new project
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("project4");
        projectTest.setDescription("project4");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);

        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint()
                    .getProjectByNameWithUserId(userSession, user.getId(), projectTest.getName());
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

    @Test
    void persisProjectNegative2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        /**
         * Creating a new project
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("project2");
        projectTest.setDescription("project2");
        projectTest.setStatus(Status.PROCESSING);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        @NotNull final Project returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(userSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}