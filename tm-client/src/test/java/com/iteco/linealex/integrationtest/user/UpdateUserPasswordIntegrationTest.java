package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.api.endpoint.User;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UpdateUserPasswordIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @NotNull
    private Session adminSession;

    @NotNull
    private Session userSession;

    @Test
    void updateUserPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(adminSession.getRole(), Role.ADMINISTRATOR);
        assertEquals(adminSession.getUserId(), admin.getId());
        @NotNull final User newUser = new User();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable User returnedUser = bootstrap.getUserEndpoint().getUserById(adminSession, newUser.getId());
        assertNotNull(returnedUser);
        bootstrap.getUserEndpoint()
                .updateUserPassword(adminSession, "12345678", "87654321", newUser);
        newUser.setHashPassword(TransformatorToHashMD5.getHash("87654321",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        returnedUser = bootstrap.getUserEndpoint().getUserById(adminSession, newUser.getId());
        assertEquals(newUser.getHashPassword(), returnedUser.getHashPassword());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserById(adminSession, newUser.getId());
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void updateUserPositive2() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(userSession.getRole(), Role.ORDINARY_USER);
        assertEquals(userSession.getUserId(), user.getId());
        bootstrap.getUserEndpoint().updateUserPassword(userSession,
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"),
                "33333333", user);
        user.setHashPassword(TransformatorToHashMD5.getHash("33333333",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        assertEquals(user.getHashPassword(), bootstrap.getUserEndpoint()
                .getUserById(userSession, user.getId()).getHashPassword());
        bootstrap.getUserEndpoint().updateUserPassword(userSession,
                "33333333", bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"), user);
        user.setHashPassword(TransformatorToHashMD5.getHash(bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"),
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        assertEquals(user.getHashPassword(), bootstrap.getUserEndpoint()
                .getUserById(userSession, user.getId()).getHashPassword());
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

    @Test
    void updateUserNegative1() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        assertSame(userSession.getRole(), Role.ORDINARY_USER);
        assertEquals(userSession.getUserId(), user.getId());
        @NotNull final User newUser = new User();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                bootstrap.getPropertyEndpoint().getProperty("PASSWORD_SALT"),
                Integer.parseInt(bootstrap.getPropertyEndpoint().getProperty("PASSWORD_TIMES"))));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().updateUserPassword(userSession, "12345678",
                    "33333333", newUser);
        });
        assertNotNull(thrown.getMessage());
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}