package com.iteco.linealex.integrationtest.task;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.util.ApplicationUtils;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.lang.Exception;
import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RemoveTaskIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @NotNull
    private Session adminSession;

    @NotNull
    private Session userSession;

    @Test
    void removeTaskPositive1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating a new project
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final Project projectTest2 = new Project();
        projectTest2.setUserId(admin.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);
        /**
         * Creating tasks
         */
        @NotNull final Task taskTest = new Task();
        taskTest.setUserId(admin.getId());
        taskTest.setProjectId(projectTest.getId());
        taskTest.setId(UUID.randomUUID().toString());
        taskTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setName("taskTest1");
        taskTest.setDescription("taskTest1");
        taskTest.setStatus(Status.PLANNED);

        @NotNull final Task taskTest2 = new Task();
        taskTest2.setUserId(admin.getId());
        taskTest2.setProjectId(projectTest.getId());
        taskTest2.setId(UUID.randomUUID().toString());
        taskTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest2.setName("taskTest2");
        taskTest2.setDescription("taskTest2");
        taskTest2.setStatus(Status.PLANNED);

        @NotNull final Task taskTest3 = new Task();
        taskTest3.setUserId(admin.getId());
        taskTest3.setProjectId(projectTest2.getId());
        taskTest3.setId(UUID.randomUUID().toString());
        taskTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest3.setName("taskTest3");
        taskTest3.setDescription("taskTest3");
        taskTest3.setStatus(Status.PLANNED);

        @NotNull final Task taskTest4 = new Task();
        taskTest4.setUserId(admin.getId());
        taskTest4.setProjectId(projectTest2.getId());
        taskTest4.setId(UUID.randomUUID().toString());
        taskTest4.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest4.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest4.setName("taskTest4");
        taskTest4.setDescription("taskTest4");
        taskTest4.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB task
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest2);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest2);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest3);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest4);

        bootstrap.getTaskEndpoint().removeTask(adminSession, taskTest.getId());
        bootstrap.getTaskEndpoint().removeTask(adminSession, taskTest2.getId());
        bootstrap.getTaskEndpoint().removeTask(adminSession, taskTest3.getId());
        bootstrap.getTaskEndpoint().removeTask(adminSession, taskTest4.getId());

        @NotNull Task returnedTask = bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest.getId());
        assertNull(returnedTask);

        returnedTask = bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest2.getId());
        assertNull(returnedTask);


        returnedTask = bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest3.getId());
        assertNull(returnedTask);

        returnedTask = bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest4.getId());
        assertNull(returnedTask);
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(adminSession, admin.getId(), projectTest.getId());
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(adminSession, admin.getId(), projectTest2.getId());
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest2.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest2.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest3.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest4.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void removeTaskPositive2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        /**
         * Creating a new project
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final Project projectTest2 = new Project();
        projectTest2.setUserId(user.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);
        /**
         * Creating tasks
         */
        @NotNull final Task taskTest = new Task();
        taskTest.setUserId(user.getId());
        taskTest.setProjectId(projectTest.getId());
        taskTest.setId(UUID.randomUUID().toString());
        taskTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setName("taskTest1");
        taskTest.setDescription("taskTest1");
        taskTest.setStatus(Status.PLANNED);

        @NotNull final Task taskTest2 = new Task();
        taskTest2.setUserId(user.getId());
        taskTest2.setProjectId(projectTest.getId());
        taskTest2.setId(UUID.randomUUID().toString());
        taskTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest2.setName("taskTest2");
        taskTest2.setDescription("taskTest2");
        taskTest2.setStatus(Status.PLANNED);

        @NotNull final Task taskTest3 = new Task();
        taskTest3.setUserId(user.getId());
        taskTest3.setProjectId(projectTest2.getId());
        taskTest3.setId(UUID.randomUUID().toString());
        taskTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest3.setName("taskTest3");
        taskTest3.setDescription("taskTest3");
        taskTest3.setStatus(Status.PLANNED);

        @NotNull final Task taskTest4 = new Task();
        taskTest4.setUserId(user.getId());
        taskTest4.setProjectId(projectTest2.getId());
        taskTest4.setId(UUID.randomUUID().toString());
        taskTest4.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest4.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest4.setName("taskTest4");
        taskTest4.setDescription("taskTest4");
        taskTest4.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB task
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest2);
        bootstrap.getTaskEndpoint().persistTask(userSession, taskTest);
        bootstrap.getTaskEndpoint().persistTask(userSession, taskTest2);
        bootstrap.getTaskEndpoint().persistTask(userSession, taskTest3);
        bootstrap.getTaskEndpoint().persistTask(userSession, taskTest4);

        bootstrap.getTaskEndpoint().removeTask(userSession, taskTest.getId());
        bootstrap.getTaskEndpoint().removeTask(userSession, taskTest2.getId());
        bootstrap.getTaskEndpoint().removeTask(userSession, taskTest3.getId());
        bootstrap.getTaskEndpoint().removeTask(userSession, taskTest4.getId());

        @NotNull Task returnedTask = bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest.getId());
        assertNull(returnedTask);

        returnedTask = bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest2.getId());
        assertNull(returnedTask);


        returnedTask = bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest3.getId());
        assertNull(returnedTask);

        returnedTask = bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest4.getId());
        assertNull(returnedTask);
        /**
         * Clean up
         */
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(userSession, user.getId(), projectTest.getId());
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(userSession, user.getId(), projectTest2.getId());
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest2.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest2.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest3.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest4.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void removeTaskNegative1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating a new project
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Creating tasks
         */
        @NotNull final Task taskTest = new Task();
        taskTest.setUserId(admin.getId());
        taskTest.setProjectId(projectTest.getId());
        taskTest.setId(UUID.randomUUID().toString());
        taskTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setName("taskTest1");
        taskTest.setDescription("taskTest1");
        taskTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB task
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getTaskEndpoint().removeTask(null, taskTest.getId());
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}