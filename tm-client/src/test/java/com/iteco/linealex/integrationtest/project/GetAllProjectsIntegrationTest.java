package com.iteco.linealex.integrationtest.project;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;

import java.lang.Exception;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GetAllProjectsIntegrationTest {

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @NotNull
    private Session adminSession;

    @NotNull
    private Session userSession;

    @Test
    void GetAllProjectsPositive1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating new projects
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final Project projectTest2 = new Project();
        projectTest2.setUserId(admin.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest2);
        @NotNull final List<Project> collection = new ArrayList<>(bootstrap
                .getProjectEndpoint().getAllProjects(adminSession));
        assertFalse(collection.isEmpty());
        collection.sort(Comparator.comparing(Project::getName));
        assertEquals(collection.get(0).getId(), projectTest.getId());
        assertEquals(collection.get(1).getId(), projectTest2.getId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllProjectsPositive2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        /**
         * Creating new projects
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final Project projectTest2 = new Project();
        projectTest2.setUserId(user.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest2);

        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint().getAllProjects(userSession);
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllProjectsNegative1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating new projects
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final Project projectTest2 = new Project();
        projectTest2.setUserId(admin.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest2);
        @NotNull final List<Project> collection = new ArrayList<>(bootstrap
                .getProjectEndpoint().getAllProjects(adminSession));
        assertFalse(collection.isEmpty());
        collection.sort(Comparator.comparing(Project::getName));
        assertNotEquals(collection.get(1).getId(), projectTest.getId());
        assertNotEquals(collection.get(0).getId(), projectTest2.getId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllProjectsNegative2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        userSession = bootstrap.getSessionEndpoint().createSession("user",
                bootstrap.getPropertyEndpoint().getProperty("USER_PASSWORD"));
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin",
                bootstrap.getPropertyEndpoint().getProperty("ADMIN_PASSWORD"));
        /**
         * Creating new projects
         */
        @NotNull final Project projectTest = new Project();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final Project projectTest2 = new Project();
        projectTest2.setUserId(user.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest2);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint().getAllProjects(userSession);
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}