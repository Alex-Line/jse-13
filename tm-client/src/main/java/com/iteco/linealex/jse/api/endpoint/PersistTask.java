package com.iteco.linealex.jse.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for persistTask complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="persistTask"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.api.jse.linealex.iteco.com/}session" minOccurs="0"/&gt;
 *         &lt;element name="task" type="{http://endpoint.api.jse.linealex.iteco.com/}task" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "persistTask", propOrder = {
        "session",
        "task"
})
public class PersistTask {

    protected Session session;
    protected Task task;

    /**
     * Gets the value of the session property.
     *
     * @return possible object is
     * {@link Session }
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value allowed object is
     *              {@link Session }
     */
    public void setSession(Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the task property.
     *
     * @return possible object is
     * {@link Task }
     */
    public Task getTask() {
        return task;
    }

    /**
     * Sets the value of the task property.
     *
     * @param value allowed object is
     *              {@link Task }
     */
    public void setTask(Task value) {
        this.task = value;
    }

}
