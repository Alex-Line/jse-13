package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.api.endpoint.IProjectEndpoint;
import com.iteco.linealex.jse.api.endpoint.ITaskEndpoint;
import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "CLEAR THE LIST OF PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        if (session.getRole() != Role.ADMINISTRATOR) {
            projectEndpoint.removeAllProjectsByUserId(session, session.getUserId());
            System.out.println("[All PROJECTS REMOVED]\n");
            return;
        } else {
            projectEndpoint.removeAllProjects(session);
            System.out.println("[All PROJECTS REMOVED]\n");
        }

        System.out.println("[THERE IS NOT ANY PROJECTS FOR REMOVING]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}