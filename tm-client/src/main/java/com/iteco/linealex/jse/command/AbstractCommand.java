package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.User;
import com.iteco.linealex.jse.context.Bootstrap;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected Bootstrap serviceLocator;

    @NotNull
    protected List<Role> roles = Arrays.asList(Role.ADMINISTRATOR, Role.ORDINARY_USER);

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;

    public boolean secure() {
        return serviceLocator.getSession() != null;
    }

    public void printUser(@NotNull final User user) {
        System.out.println("User " + user.getLogin() +
                ",\n     role = " + user.getRole() +
                ",\n     id = " + user.getId() +
                ",\n     hashPassword = " + user.getHashPassword()
        );
    }

}