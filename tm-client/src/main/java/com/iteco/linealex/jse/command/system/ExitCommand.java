package com.iteco.linealex.jse.command.system;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "EXIT FROM APPLICATION";
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

    @Override
    public boolean secure() {
        return false;
    }

}