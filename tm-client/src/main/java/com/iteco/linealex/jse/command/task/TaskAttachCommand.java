package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.api.endpoint.Project;
import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.api.endpoint.Task;
import com.iteco.linealex.jse.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskAttachCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-attach";
    }

    @NotNull
    @Override
    public String description() {
        return "ATTACH THE SELECTED TASK TO PROJECT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            System.out.println("[YOU DID NOT SELECT ANY TASK! SELECT ANY AND TRY AGAIN]\n");
            return;
        }
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        if (project == null) throw new TaskManagerException_Exception();
        System.out.println("[ENTER TASK NAME]");
        @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().getTaskByName(session, taskName);
        serviceLocator.getTaskEndpoint().attachTaskToProject(session, project.getId(), task);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}