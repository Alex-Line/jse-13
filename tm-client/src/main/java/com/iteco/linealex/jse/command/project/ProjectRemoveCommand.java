package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.api.endpoint.Project;
import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVING A PROJECT BY NAME";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        @Nullable Project project = null;
        if (session.getRole() == Role.ADMINISTRATOR)
            project = serviceLocator.getProjectEndpoint().getProjectByName(session, projectName);
        else project = serviceLocator.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        if (project == null) {
            System.out.println("[THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!]\n");
            return;
        }
        serviceLocator.getProjectEndpoint().removeProject(session, project.getId());
        System.out.println("[REMOVING PROJECT]");
        serviceLocator.getTaskEndpoint().removeAllTasksFromProject(session, session.getUserId(), project.getId());
        System.out.println("[REMOVING TASKS FROM PROJECT]");
        System.out.println("[ALL TASKS REMOVED]");
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}