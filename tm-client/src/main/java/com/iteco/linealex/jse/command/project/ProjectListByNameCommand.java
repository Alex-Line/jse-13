package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.api.endpoint.Project;
import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class ProjectListByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list-name";
    }

    @NotNull
    @Override
    public String description() {
        return "LIST PROJECTS BY PART OF NAME OR DESCRIPTION";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        System.out.println("ENTER PART OF NAME OR DESCRIPTION TO SEARCH");
        @NotNull final String pattern = serviceLocator.getTerminalService().nextLine();
        System.out.println("[PROJECT LIST]");
        @NotNull Collection<Project> collection = Collections.EMPTY_LIST;
        if (session.getRole() == Role.ADMINISTRATOR) {
            collection = serviceLocator.getProjectEndpoint().getAllProjectsByName(session, pattern);
        } else collection = serviceLocator.getProjectEndpoint()
                .getAllProjectsByNameWithUserId(session, session.getUserId(), pattern);
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (@NotNull final Project project : collection) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}