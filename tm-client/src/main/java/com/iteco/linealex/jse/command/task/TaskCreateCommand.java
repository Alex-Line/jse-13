package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "CREATE A NEW TASK IN PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        @Nullable final User user = serviceLocator.getUserEndpoint().getUserById(session, session.getUserId());
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        @NotNull final Task task = new Task();
        task.setUserId(session.getUserId());
        System.out.println("[ENTER TASK NAME]");
        @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
        task.setName(taskName);
        System.out.println("[ENTER TASK DESCRIPTION]");
        @NotNull final String taskDescription = serviceLocator.getTerminalService().nextLine();
        task.setDescription(taskDescription);
        System.out.println("[ENTER TASK START DATE IN FORMAT: DD.MM.YYYY]");
        task.setDateStart(ApplicationUtils.toXMLGregorianCalendar(
                ApplicationUtils.formatStringToDate(serviceLocator.getTerminalService().nextLine())));
        System.out.println("[ENTER TASK FINISH DATE IN FORMAT: DD.MM.YYYY]");
        task.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(
                ApplicationUtils.formatStringToDate(serviceLocator.getTerminalService().nextLine())));
        if (project == null) task.setProjectId(null);
        else task.setProjectId(project.getId());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}