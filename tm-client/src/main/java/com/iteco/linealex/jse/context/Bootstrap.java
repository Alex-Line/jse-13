package com.iteco.linealex.jse.context;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.endpoint.*;
import com.iteco.linealex.jse.service.TerminalService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.lang.Exception;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class Bootstrap {

    @NotNull
    private final TerminalService terminalService = new TerminalService();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    private Session session;

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final IPropertyEndpoint propertyEndpoint = new PropertyEndpointService().getPropertyEndpointPort();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();


    public void start() {
        try {
            init();
            System.out.println("*** WELCOME TO TASK MANAGER ***");
            @NotNull String command = "";
            while (!"exit".equals(command)) {
                command = terminalService.nextLine().toLowerCase();
                try {
                    execute(command);
                } catch (TaskManagerException_Exception taskException) {
                    System.out.println(taskException.getMessage());
                }
            }
        } catch (TaskManagerException_Exception | NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("com.iteco.linealex.jse.command")
                        .getSubTypesOf(com.iteco.linealex.jse.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            registry(clazz.newInstance());
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        @NotNull final String commandName = command.command();
        if (commandName.isEmpty()) return;
        @NotNull final String commandDescription = command.description();
        if (commandDescription.isEmpty()) return;
        command.setServiceLocator(this);
        commands.put(command.command(), command);
    }


    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

}