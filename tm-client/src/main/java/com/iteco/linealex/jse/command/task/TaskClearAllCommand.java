package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskClearAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-clear-all";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVE ALL TASK EVERYWHERE";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        serviceLocator.getTaskEndpoint().removeAllTasksWithUserId(session, session.getUserId());
        System.out.println("[ALL TASKS FROM ALL PROJECTS ARE REMOVED]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}