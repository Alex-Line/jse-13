package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.jse.api.endpoint.User;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-show";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ANY USER BY LOGIN. (AVAILABLE FOR ADMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            System.out.println("[YOU MUST BE AUTHORISED TO DO THAT]\n");
            return;
        }
        @Nullable final User selectedUser = serviceLocator.getUserEndpoint().getUserById(session, session.getUserId());
        if (selectedUser == null) throw new TaskManagerException_Exception();
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        if (session.getRole() == Role.ADMINISTRATOR) {
            @Nullable final User user = serviceLocator.getUserEndpoint().getUserByLogin(session, login, selectedUser);
            printUser(user);
            System.out.println();
            return;
        }
        if (login.equals(selectedUser.getLogin())) {
            printUser(selectedUser);
            System.out.println();
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}