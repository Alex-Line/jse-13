package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.api.endpoint.Session;
import com.iteco.linealex.jse.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.jse.api.endpoint.User;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-update";
    }

    @NotNull
    @Override
    public String description() {
        return "UPDATE USER'S PASSWORD";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        @Nullable final User user = serviceLocator.getUserEndpoint().getUserById(session, session.getUserId());
        System.out.println("ENTER OLD PASSWORD");
        @NotNull final String oldPassword = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER NEW PASSWORD");
        @NotNull final String newPassword = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER NEW PASSWORD AGAIN");
        if (newPassword.equals(serviceLocator.getTerminalService().nextLine())) {
            serviceLocator.getUserEndpoint().updateUserPassword(session, oldPassword, newPassword, user);
            System.out.println("[OK]\n");
        } else System.out.println("MISTAKE IN THE REPEATING OF NEW PASSWORD\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}