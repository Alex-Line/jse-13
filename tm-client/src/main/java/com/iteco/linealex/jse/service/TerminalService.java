package com.iteco.linealex.jse.service;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public final class TerminalService {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    @NotNull
    public String nextLine() {
        return scanner.nextLine().trim();
    }

}