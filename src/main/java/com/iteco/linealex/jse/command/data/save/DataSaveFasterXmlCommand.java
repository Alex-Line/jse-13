package com.iteco.linealex.jse.command.data.save;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.constant.Constants;
import com.iteco.linealex.jse.dto.Domain;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class DataSaveFasterXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-faster-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE DATA INTO XML FILE BY FASTER-XML";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator);
        @NotNull final File saveDir = new File(Constants.SAVE_DIR);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(Constants.SAVE_DIR + "data.xml");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}