package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.UserIsNotLogInException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public class TaskListByFinishDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list-finish";
    }

    @NotNull
    @Override
    public String description() {
        return "LIST TASKS BY FINISH DATE";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) throw new UserIsNotLogInException();
        @Nullable final Project selectedProject = serviceLocator.getSelectedEntityService().getSelectedProject();
        System.out.println("[TASK LIST]");
        @NotNull Collection<Task> collection = Collections.EMPTY_LIST;
        if (selectedUser.getRole() == Role.ADMINISTRATOR)
            collection = serviceLocator.getTaskService().getEntitiesSortedByFinishDate();
        else if (selectedProject != null)
            collection = serviceLocator.getTaskService().getEntitiesSortedByFinishDate(
                    selectedProject.getId(), selectedUser.getId());
        else collection = serviceLocator.getTaskService().getEntitiesSortedByFinishDate(
                    selectedUser.getId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (Task task : collection) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}