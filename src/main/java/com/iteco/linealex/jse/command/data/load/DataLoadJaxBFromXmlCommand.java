package com.iteco.linealex.jse.command.data.load;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.constant.Constants;
import com.iteco.linealex.jse.dto.Domain;
import com.iteco.linealex.jse.exception.ThereIsNotSuchFileException;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Collections;

public class DataLoadJaxBFromXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-jaxb-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD DATA FROM XML FILE BY JAXB";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getSelectedEntityService().clearSelection();
        serviceLocator.getSelectedEntityService().setSelectedUser(null);
        @NotNull final File savedFile = new File(Constants.SAVE_DIR + "data.xml");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(savedFile);
        if (domain.getUsers() == null) serviceLocator.getUserService().persist(Collections.EMPTY_LIST);
        else serviceLocator.getUserService().persist(domain.getUsers());
        if (domain.getProjects() == null) serviceLocator.getProjectService().persist(Collections.EMPTY_LIST);
        else serviceLocator.getProjectService().persist(domain.getProjects());
        if (domain.getTasks() == null) serviceLocator.getTaskService().persist(Collections.EMPTY_LIST);
        else serviceLocator.getTaskService().persist(domain.getTasks());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}