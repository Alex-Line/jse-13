package com.iteco.linealex.jse.command.data.save;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.constant.Constants;
import com.iteco.linealex.jse.dto.Domain;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public class DataSaveJaxBToXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-jaxb-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE DATA INTO XML FILE BY JAXB";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator);
        @NotNull final File saveDir = new File(Constants.SAVE_DIR);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(Constants.SAVE_DIR + "data.xml");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}