package com.iteco.linealex.jse.context;

import com.iteco.linealex.jse.api.service.*;
import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.LowAccessLevelException;
import com.iteco.linealex.jse.exception.TaskManagerException;
import com.iteco.linealex.jse.exception.UserIsNotLogInException;
import com.iteco.linealex.jse.repository.CommandRepository;
import com.iteco.linealex.jse.repository.ProjectRepository;
import com.iteco.linealex.jse.repository.TaskRepository;
import com.iteco.linealex.jse.repository.UserRepository;
import com.iteco.linealex.jse.service.*;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.security.NoSuchAlgorithmException;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final IProjectService<Project> projectService = new ProjectService(new ProjectRepository());

    @NotNull
    private final ITaskService<Task> taskService = new TaskService(new TaskRepository());

    @NotNull
    private final ICommandService commandService = new CommandService(new CommandRepository());

    @NotNull
    private final IUserService<User> userService = new UserService(new UserRepository());

    @NotNull
    private final ISelectedEntityService selectedEntityService = new SelectedEntityService();

    private void registry(@NotNull final AbstractCommand command) {
        @NotNull final String commandName = command.command();
        if (commandName.isEmpty()) return;
        @NotNull final String commandDescription = command.description();
        if (commandDescription.isEmpty()) return;
        command.setServiceLocator(this);
        commandService.addCommand(command);
    }

    public void start() {
        try {
            createInitialUsers();
            init();
            System.out.println("*** WELCOME TO TASK MANAGER ***");
            @NotNull String command = "";
            while (!"exit".equals(command)) {
                command = terminalService.nextLine().toLowerCase();
                try {
                    execute(command);
                } catch (TaskManagerException taskException) {
                    System.out.println(taskException.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("com.iteco.linealex.jse.command").getSubTypesOf(com.iteco.linealex.jse.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            registry(clazz.newInstance());
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommand(command);
        if (abstractCommand == null) return;
        if (abstractCommand.secure() && getSelectedEntityService().getSelectedUser() == null)
            throw new UserIsNotLogInException();
        if (getSelectedEntityService().getSelectedUser() != null &&
                !abstractCommand.getAvailableRoles().contains(getSelectedEntityService().getSelectedUser().getRole())) {
            System.out.println("[THIS COMMAND AVAILABLE FOR USER ACCESS LEVEL :]");
            System.out.println(abstractCommand.getAvailableRoles());
            System.out.println("[YOUR LEVEL IS :]");
            System.out.println(getSelectedEntityService().getSelectedUser().getRole());
            throw new LowAccessLevelException();
        }
        abstractCommand.execute();
    }

    private void createInitialUsers() throws TaskManagerException, NoSuchAlgorithmException {
        @NotNull final User admin = new User("admin", TransformatorToHashMD5.getHash("11111111"));
        admin.setRole(Role.ADMINISTRATOR);
        getSelectedEntityService().setSelectedUser(admin);
        userService.createUser(admin, getSelectedEntityService().getSelectedUser());
        getSelectedEntityService().setSelectedUser(admin);
        userService.createUser(new User("startuser", TransformatorToHashMD5.getHash("22222222")),
                getSelectedEntityService().getSelectedUser());
    }

    @NotNull
    @Override
    public IProjectService<Project> getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService<Task> getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService<User> getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public ITerminalService getTerminalService() {
        return terminalService;
    }

    @NotNull
    @Override
    public ISelectedEntityService getSelectedEntityService() {
        return selectedEntityService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

}