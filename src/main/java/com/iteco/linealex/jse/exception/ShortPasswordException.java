package com.iteco.linealex.jse.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class ShortPasswordException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "ENTERED PASSWORD IS TO SHORT. COMMAND WAS INTERRUPTED\n";
    }

}