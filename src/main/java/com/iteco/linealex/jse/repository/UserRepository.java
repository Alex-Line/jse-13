package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.repository.IRepository;
import com.iteco.linealex.jse.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public final class UserRepository extends AbstractRepository<User> implements IRepository<User> {

    @Override
    public boolean contains(
            @NotNull final String entityName,
            @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, User> entry : entityMap.entrySet()) {
            if (entry.getValue().getName() == null) continue;
            if (entry.getValue().getName().equals(entityName)) return true;
        }
        return false;
    }

    public boolean contains(@NotNull final String login) {
        for (@NotNull final Map.Entry<String, User> entry : entityMap.entrySet()) {
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(login)) continue;
            return true;
        }
        return false;
    }

    @NotNull
    @Override
    public Collection<User> findAll(@NotNull final String userId) {
        @NotNull final Collection<User> collection = new ArrayList<>();
        if (userId.isEmpty()) return collection;
        for (@NotNull final Map.Entry<String, User> entry : entityMap.entrySet()) {
            if (entry.getValue().getId().equals(userId))
                collection.add(entry.getValue());
        }
        return collection;
    }

    @Nullable
    @Override
    public User merge(@NotNull final User example) {
        if (example.getName() == null) return null;
        @Nullable final User user = findOne(example.getName());
        if (user == null) return null;
        user.setRole(example.getRole());
        user.setHashPassword(example.getHashPassword());
        return user;
    }

    @NotNull
    @Override
    public Collection<User> findAllByName(@NotNull String pattern) {
        return entityMap.values().stream().filter(e ->
                (e.getName() != null && e.getName().contains(pattern)))
                .collect(Collectors.toList());
    }

}